package ai.turbochain.ipex.dao;

import ai.turbochain.ipex.constant.Platform;
import ai.turbochain.ipex.dao.base.BaseDao;
import ai.turbochain.ipex.entity.AppRevision;

/**
 * @author jack
 * @Title: 
 * @Description:
 * @date 2020/4/2416:18
 */
public interface AppRevisionDao extends BaseDao<AppRevision> {
    AppRevision findAppRevisionByPlatformOrderByIdDesc(Platform platform);
}
