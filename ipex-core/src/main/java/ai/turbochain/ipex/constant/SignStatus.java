package ai.turbochain.ipex.constant;

/**
 * @author jack
 * @Description:
 * @date 2020/5/311:33
 */
public enum SignStatus {
    UNDERWAY, FINISH;
}
