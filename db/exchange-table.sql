

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enable` int(11) NULL DEFAULT NULL,
  `last_login_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_login_time` datetime(0) NULL DEFAULT NULL,
  `mobile_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qq` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `real_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `department_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_gfn44sntic2k93auag97juyij`(`username`) USING BTREE,
  INDEX `FKibjnyhe6m46qfkc6vgbir1ucq`(`department_id`) USING BTREE,
  CONSTRAINT `FKnmmt6f2kg0oaxr11uhy7qqf3w` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_ibfk_3` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for admin_access_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_access_log`;
CREATE TABLE `admin_access_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `access_method` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `access_time` datetime(0) NULL DEFAULT NULL,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `module` int(11) NULL DEFAULT NULL,
  `operation` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8800 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for admin_permission
-- ----------------------------
DROP TABLE IF EXISTS `admin_permission`;
CREATE TABLE `admin_permission`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` bigint(20) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 190 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for admin_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 87 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for admin_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_permission`;
CREATE TABLE `admin_role_permission`  (
  `role_id` bigint(20) NOT NULL,
  `rule_id` bigint(20) NOT NULL,
  UNIQUE INDEX `UKplesprlvm1sob8nl9yc5rgh3m`(`role_id`, `rule_id`) USING BTREE,
  INDEX `FK52rddd3qje4p49iubt08gplb5`(`role_id`) USING BTREE,
  INDEX `FKqf3fhgl5mjqqb0jeupx7yafh0`(`rule_id`) USING BTREE,
  CONSTRAINT `FK52rddd3qje4p49iubt08gplb5` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKqf3fhgl5mjqqb0jeupx7yafh0` FOREIGN KEY (`rule_id`) REFERENCES `admin_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_role_permission_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_role_permission_ibfk_2` FOREIGN KEY (`rule_id`) REFERENCES `admin_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_role_permission_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `admin_role_permission_ibfk_4` FOREIGN KEY (`rule_id`) REFERENCES `admin_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for advertise
-- ----------------------------
DROP TABLE IF EXISTS `advertise`;
CREATE TABLE `advertise`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `advertise_type` int(11) NOT NULL,
  `auto` int(11) NULL DEFAULT NULL,
  `autoword` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `coin_unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `deal_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '交易中数量',
  `level` int(11) NULL DEFAULT NULL,
  `limit_money` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `max_limit` decimal(18, 2) NULL DEFAULT NULL COMMENT '最高单笔交易额',
  `min_limit` decimal(18, 2) NULL DEFAULT NULL COMMENT '最低单笔交易额',
  `number` decimal(18, 8) NULL DEFAULT NULL COMMENT '计划数量',
  `pay_mode` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `premise_rate` decimal(18, 6) NULL DEFAULT NULL COMMENT '溢价百分比',
  `price` decimal(18, 2) NULL DEFAULT NULL COMMENT '交易价格',
  `price_type` int(11) NOT NULL,
  `remain_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '计划剩余数量',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `time_limit` int(11) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `version` bigint(20) NULL DEFAULT NULL,
  `coin_id` bigint(20) NOT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `origin` tinyint(1) NULL DEFAULT 0 COMMENT '1代表IPcon手机端,2 ：HardID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK75rse9iecdnimf8ugtf20c43l`(`coin_id`) USING BTREE,
  CONSTRAINT `FK75rse9iecdnimf8ugtf20c43l` FOREIGN KEY (`coin_id`) REFERENCES `otc_coin` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for announcement
-- ----------------------------
DROP TABLE IF EXISTS `announcement`;
CREATE TABLE `announcement`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `is_show` bit(1) NULL DEFAULT NULL,
  `is_top` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for app_revision
-- ----------------------------
DROP TABLE IF EXISTS `app_revision`;
CREATE TABLE `app_revision`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `download_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `platform` int(11) NULL DEFAULT NULL,
  `publish_time` datetime(0) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for appeal
-- ----------------------------
DROP TABLE IF EXISTS `appeal`;
CREATE TABLE `appeal`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `associate_id` bigint(20) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `deal_with_time` datetime(0) NULL DEFAULT NULL,
  `initiator_id` bigint(20) NULL DEFAULT NULL,
  `is_success` int(11) NULL DEFAULT NULL,
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `order_id` bigint(20) NOT NULL,
  `images` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_todwxorutclquf69bwow70kml`(`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for business_auth_apply
-- ----------------------------
DROP TABLE IF EXISTS `business_auth_apply`;
CREATE TABLE `business_auth_apply`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19, 2) NULL DEFAULT NULL,
  `auditing_time` datetime(0) NULL DEFAULT NULL,
  `auth_info` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `certified_business_status` int(11) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `deposit_record_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `business_auth_deposit_id` bigint(20) NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for business_auth_deposit
-- ----------------------------
DROP TABLE IF EXISTS `business_auth_deposit`;
CREATE TABLE `business_auth_deposit`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '保证金数额',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bussiness_cancel_apply
-- ----------------------------
DROP TABLE IF EXISTS `bussiness_cancel_apply`;
CREATE TABLE `bussiness_cancel_apply`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cancel_apply_time` datetime(0) NULL DEFAULT NULL,
  `deposit_record_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `handle_time` datetime(0) NULL DEFAULT NULL,
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for coin
-- ----------------------------
DROP TABLE IF EXISTS `coin`;
CREATE TABLE `coin`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `can_auto_withdraw` int(11) NULL DEFAULT NULL,
  `can_recharge` int(11) NULL DEFAULT NULL,
  `can_transfer` int(11) NULL DEFAULT NULL,
  `can_withdraw` int(11) NULL DEFAULT NULL,
  `cny_rate` double NOT NULL,
  `enable_rpc` int(11) NULL DEFAULT NULL,
  `is_platform_coin` int(11) NULL DEFAULT NULL,
  `max_tx_fee` double NOT NULL,
  `max_withdraw_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '最大提币数量',
  `min_tx_fee` double NOT NULL,
  `min_withdraw_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '最小提币数量',
  `name_cn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `usd_rate` double NOT NULL,
  `withdraw_threshold` decimal(18, 8) NULL DEFAULT NULL COMMENT '提现阈值',
  `has_legal` bit(1) NOT NULL DEFAULT b'0',
  `cold_wallet_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `miner_fee` decimal(18, 8) NULL DEFAULT 0.00000000 COMMENT '矿工费',
  `withdraw_scale` int(11) NULL DEFAULT 4 COMMENT '提币精度',
  `is_token` int(2) NULL DEFAULT 0 COMMENT '是否是代币:0,不是;1,是',
  `chain_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '代币所在主链名',
  `token_address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '代币地址',
  `decimals` int(11) NULL DEFAULT 18 COMMENT '代币单位',
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country`  (
  `zh_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `area_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `en_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `language` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `local_currency` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`zh_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for data_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `data_dictionary`;
CREATE TABLE `data_dictionary`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bond` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `creation_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `leader_id` bigint(20) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_1t68827l97cwyxo9r1u6t4p7d`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for deposit_record
-- ----------------------------
DROP TABLE IF EXISTS `deposit_record`;
CREATE TABLE `deposit_record`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `amount` decimal(19, 2) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dividend_start_record
-- ----------------------------
DROP TABLE IF EXISTS `dividend_start_record`;
CREATE TABLE `dividend_start_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(18, 6) NULL DEFAULT NULL COMMENT '数量',
  `date` datetime(0) NULL DEFAULT NULL,
  `end` bigint(20) NULL DEFAULT NULL,
  `end_date` datetime(0) NULL DEFAULT NULL,
  `rate` decimal(18, 2) NULL DEFAULT NULL COMMENT '比例',
  `start` bigint(20) NULL DEFAULT NULL,
  `start_date` datetime(0) NULL DEFAULT NULL,
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for exchange_coin
-- ----------------------------
DROP TABLE IF EXISTS `exchange_coin`;
CREATE TABLE `exchange_coin`  (
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `base_coin_scale` int(11) NOT NULL,
  `base_symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `coin_scale` int(11) NOT NULL,
  `coin_symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enable` int(11) NOT NULL,
  `fee` decimal(8, 4) NULL DEFAULT NULL COMMENT '交易手续费',
  `sort` int(11) NOT NULL,
  `enable_market_sell` int(11) NULL DEFAULT 1 COMMENT '是否启用市价卖',
  `enable_market_buy` int(11) NULL DEFAULT 1 COMMENT '是否启用市价买',
  `min_sell_price` decimal(18, 8) NULL DEFAULT 0.00000000 COMMENT '最低挂单卖价',
  `flag` int(11) NULL DEFAULT 0,
  `max_trading_order` int(11) NULL DEFAULT 0 COMMENT '最大允许同时交易的订单数，0表示不限制',
  `max_trading_time` int(11) NULL DEFAULT 0 COMMENT '委托超时自动下架时间，单位为秒，0表示不过期',
  `instrument` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交易类型，B2C2特有',
  `min_turnover` decimal(18, 8) NOT NULL DEFAULT 0.00000000 COMMENT '最小挂单成交额',
  `max_volume` decimal(18, 8) NULL DEFAULT 0.00000000 COMMENT '最大下单量',
  `min_volume` decimal(18, 8) NULL DEFAULT 0.00000000 COMMENT '最小下单量',
  `zone` int(11) NULL DEFAULT 0,
  `source` int(1) NULL DEFAULT NULL COMMENT '来源 0代表ipex 2:hardId',
  PRIMARY KEY (`symbol`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for exchange_coin_settlement
-- ----------------------------
DROP TABLE IF EXISTS `exchange_coin_settlement`;
CREATE TABLE `exchange_coin_settlement`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enable` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for exchange_favor_symbol
-- ----------------------------
DROP TABLE IF EXISTS `exchange_favor_symbol`;
CREATE TABLE `exchange_favor_symbol`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `add_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 292 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for exchange_order
-- ----------------------------
DROP TABLE IF EXISTS `exchange_order`;
CREATE TABLE `exchange_order`  (
  `order_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `amount` decimal(18, 8) NULL DEFAULT 0.00000000,
  `base_symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `canceled_time` bigint(20) NULL DEFAULT NULL,
  `coin_symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `completed_time` bigint(20) NULL DEFAULT NULL,
  `direction` int(11) NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  `price` decimal(18, 8) NULL DEFAULT 0.00000000,
  `status` int(11) NULL DEFAULT NULL,
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `time` bigint(20) NULL DEFAULT NULL,
  `traded_amount` decimal(26, 16) NULL DEFAULT 0.0000000000000000,
  `turnover` decimal(26, 16) NULL DEFAULT 0.0000000000000000,
  `type` int(11) NULL DEFAULT NULL,
  `use_discount` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `source` int(1) NULL DEFAULT NULL COMMENT '来源 0代表ipex 2:hardId',
  PRIMARY KEY (`order_id`) USING BTREE,
  UNIQUE INDEX `INDEX_ID`(`order_id`) USING BTREE,
  INDEX `INDEX_USER`(`member_id`, `status`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for financial_item
-- ----------------------------
DROP TABLE IF EXISTS `financial_item`;
CREATE TABLE `financial_item`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `coin_minnum` decimal(19, 2) NULL DEFAULT NULL,
  `coin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `deadline` int(11) NOT NULL,
  `item_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `item_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `item_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `item_state` int(11) NOT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `yield` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for financial_order
-- ----------------------------
DROP TABLE IF EXISTS `financial_order`;
CREATE TABLE `financial_order`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `coin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `coin_num` decimal(19, 2) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `frozen_days` int(11) NOT NULL,
  `item_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  `order_no` bigint(20) NULL DEFAULT NULL,
  `order_state` int(11) NOT NULL,
  `order_usdt_rate` double NULL DEFAULT NULL,
  `plan_revenue_time` datetime(0) NULL DEFAULT NULL,
  `real_income` decimal(19, 2) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for hot_transfer_record
-- ----------------------------
DROP TABLE IF EXISTS `hot_transfer_record`;
CREATE TABLE `hot_transfer_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `amount` decimal(18, 8) NULL DEFAULT 0.00000000 COMMENT '转账金额',
  `balance` decimal(18, 8) NULL DEFAULT 0.00000000 COMMENT '热钱包余额',
  `cold_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `miner_fee` decimal(18, 8) NULL DEFAULT 0.00000000 COMMENT '矿工费',
  `transaction_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `transfer_time` datetime(0) NULL DEFAULT NULL,
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for init_plate
-- ----------------------------
DROP TABLE IF EXISTS `init_plate`;
CREATE TABLE `init_plate`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `final_price` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `init_price` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `interference_factor` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `relative_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for legal_wallet_recharge
-- ----------------------------
DROP TABLE IF EXISTS `legal_wallet_recharge`;
CREATE TABLE `legal_wallet_recharge`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(18, 2) NOT NULL COMMENT '充值金额',
  `creation_time` datetime(0) NULL DEFAULT NULL,
  `deal_time` datetime(0) NULL DEFAULT NULL,
  `pay_mode` int(11) NOT NULL,
  `payment_instrument` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `state` int(11) NOT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `coin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for legal_wallet_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `legal_wallet_withdraw`;
CREATE TABLE `legal_wallet_withdraw`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '申请总数量',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `deal_time` datetime(0) NULL DEFAULT NULL,
  `pay_mode` int(11) NOT NULL,
  `payment_instrument` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `remit_time` datetime(0) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `coin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_advertise
-- ----------------------------
DROP TABLE IF EXISTS `loan_advertise`;
CREATE TABLE `loan_advertise`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `advertise_type` int(11) NOT NULL COMMENT '广告类型',
  `coin_unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '借币标识',
  `number` decimal(18, 8) NOT NULL COMMENT '计划数量',
  `remain_amount` decimal(18, 8) NOT NULL COMMENT '计划剩余数量',
  `deal_amount` decimal(18, 8) NOT NULL DEFAULT 0.00000000 COMMENT '交易中数量',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '收款地址',
  `time_limit` int(11) NULL DEFAULT NULL COMMENT '借币天数',
  `day_rate` decimal(18, 8) NULL DEFAULT NULL COMMENT '日利率',
  `annual_rate` decimal(18, 8) NULL DEFAULT NULL COMMENT '年化利率',
  `pledge_coin_unit` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '质押借币标识',
  `pledge_number` decimal(18, 8) NULL DEFAULT NULL COMMENT '质押数量',
  `pledge_rate` decimal(18, 8) NULL DEFAULT NULL COMMENT '质押率',
  `max_limit` decimal(18, 8) NULL DEFAULT NULL COMMENT '最高单笔交易额',
  `min_limit` decimal(18, 8) NULL DEFAULT NULL COMMENT '最低单笔交易额',
  `interest` decimal(18, 8) NULL DEFAULT NULL COMMENT '利息',
  `service_charge` decimal(18, 8) NULL DEFAULT NULL COMMENT '服务费',
  `member_id` bigint(20) NOT NULL COMMENT '广告发布者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '发布时间',
  `status` int(11) NULL DEFAULT NULL COMMENT '0：上架 1：下架 2：已关闭 3：已还款 ',
  `over_due` int(1) NULL DEFAULT NULL COMMENT '是否逾期 0:否，1：是',
  `auto_liquidation` int(1) NULL DEFAULT NULL COMMENT '自动平仓 0:否，1：是',
  `early_repay` int(1) NULL DEFAULT NULL COMMENT '提前还款 0:否，1：是',
  `repay_time` datetime(0) NULL DEFAULT NULL COMMENT '还款时间',
  `close_time` datetime(0) NULL DEFAULT NULL COMMENT '关闭时间',
  `separate_orders` tinyint(1) NULL DEFAULT NULL COMMENT '是否支持分单',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '广告最后更新时间',
  `version` int(11) NULL DEFAULT 1,
  `initial_interest_rate` decimal(18, 8) NULL DEFAULT NULL COMMENT '质押币初始利率',
  `overdue_number` int(11) NOT NULL COMMENT '逾期笔数',
  `loan_number` int(11) NOT NULL COMMENT '借款笔数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 119 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_advertise_wallet_log
-- ----------------------------
DROP TABLE IF EXISTS `loan_advertise_wallet_log`;
CREATE TABLE `loan_advertise_wallet_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL COMMENT '用户编号',
  `advertise_id` bigint(20) NOT NULL COMMENT '广告编号',
  `coin_id` bigint(20) NOT NULL COMMENT '保证金币种编号',
  `amount` decimal(18, 8) NOT NULL COMMENT '冻结数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_appeal
-- ----------------------------
DROP TABLE IF EXISTS `loan_appeal`;
CREATE TABLE `loan_appeal`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `associate_id` bigint(20) NULL DEFAULT NULL COMMENT '申诉关联者',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `deal_with_time` datetime(0) NULL DEFAULT NULL,
  `initiator_id` bigint(20) NULL DEFAULT NULL COMMENT '申诉发起者',
  `is_success` int(11) NULL DEFAULT NULL,
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `order_id` bigint(20) NOT NULL,
  `images` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `UK_todwxorutclquf69bwow70kml`(`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_borrowing
-- ----------------------------
DROP TABLE IF EXISTS `loan_borrowing`;
CREATE TABLE `loan_borrowing`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键自增（借贷额度表）',
  `member_id` bigint(20) NOT NULL COMMENT '用户编号',
  `used_quota` decimal(26, 16) NOT NULL COMMENT '已使用的借款额度',
  `quota` decimal(26, 16) NOT NULL DEFAULT 0.0000000000000000 COMMENT '可使用的借款额度',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_coin
-- ----------------------------
DROP TABLE IF EXISTS `loan_coin`;
CREATE TABLE `loan_coin`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键自增（借贷币种表）',
  `unit` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '币种单位',
  `jy_rate` decimal(18, 8) NOT NULL COMMENT '交易手续费率',
  `sort` int(11) NOT NULL COMMENT '排序',
  `min_loan_amount` decimal(18, 8) NOT NULL COMMENT '最小借贷量',
  `status` int(2) NOT NULL DEFAULT 0 COMMENT '状态【0:上架；1:下架】',
  `is_loan` tinyint(1) NULL DEFAULT NULL COMMENT '是否支持借款',
  `is_pledge` tinyint(1) NULL DEFAULT NULL COMMENT '是否可以抵押',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_fee_rate
-- ----------------------------
DROP TABLE IF EXISTS `loan_fee_rate`;
CREATE TABLE `loan_fee_rate`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '借贷服务费率',
  `is_used` int(1) NOT NULL DEFAULT 0 COMMENT '是否可用【0：可用；1：不可用】',
  `symbol` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '服务费标识',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `unit` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `rate` decimal(18, 2) NULL DEFAULT NULL COMMENT '利率',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_guarantee
-- ----------------------------
DROP TABLE IF EXISTS `loan_guarantee`;
CREATE TABLE `loan_guarantee`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键自增（保证金币种表）',
  `coin_unit` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '币种单位',
  `pledge_rate` decimal(18, 2) NOT NULL COMMENT '币种质押率',
  `warn_line` decimal(18, 2) NOT NULL COMMENT '预警线',
  `close_line` decimal(18, 2) NULL DEFAULT NULL COMMENT '平仓线',
  `sort` int(1) NOT NULL DEFAULT 0 COMMENT '扣款顺序：值越小越先扣款',
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '状态【0:上架；1:下架】',
  `min_pledge_rate` decimal(18, 2) NULL DEFAULT NULL COMMENT '币种质押率下限',
  `max_pledge_rate` decimal(18, 2) NULL DEFAULT NULL COMMENT '币种质押率上限',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `loan_guarantee_coin_id`(`coin_unit`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_member_address
-- ----------------------------
DROP TABLE IF EXISTS `loan_member_address`;
CREATE TABLE `loan_member_address`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `sort` int(1) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_order
-- ----------------------------
DROP TABLE IF EXISTS `loan_order`;
CREATE TABLE `loan_order`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '订单号',
  `advertise_id` bigint(20) NOT NULL COMMENT '广告ID',
  `coin_unit` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '借贷币种',
  `number` decimal(18, 8) NOT NULL COMMENT '交易数量',
  `advertise_member_id` bigint(20) NOT NULL COMMENT '广告发起人',
  `customer_id` bigint(20) NULL DEFAULT NULL COMMENT '交易人员',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `status` int(11) NOT NULL COMMENT '订单状态0：已取消 1：待付款 2：已付款 3：已完成借贷 4：申诉中 5：已还款 6：已完成还贷',
  `pay_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '收款地址',
  `repay_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '还款地址',
  `service_charge` decimal(18, 8) NULL DEFAULT NULL COMMENT '服务费',
  `time_limit` int(11) NULL DEFAULT NULL,
  `cancel_time` datetime(0) NULL DEFAULT NULL COMMENT '订单取消时间',
  `pay_time` datetime(0) NULL DEFAULT NULL COMMENT '借款支付时间',
  `release_time` datetime(0) NULL DEFAULT NULL COMMENT '收款确认时间',
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `customer_real_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `max_limit` decimal(18, 8) NULL DEFAULT NULL COMMENT '最高交易额',
  `member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `member_real_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `version` bigint(20) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后更新时间',
  `interest` decimal(18, 8) NULL DEFAULT NULL COMMENT '预期利息',
  `income` decimal(18, 8) NULL DEFAULT NULL COMMENT '预期收益',
  `is_appeal` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否申诉中 0：否 1：是',
  `over_due` int(1) NULL DEFAULT 0 COMMENT '是否逾期 0:否，1：是',
  `auto_liquidation` int(1) NULL DEFAULT 0 COMMENT '自动平仓 0:否，1：是',
  `early_repay` int(1) NULL DEFAULT 0 COMMENT '提前还款 0:否，1：是',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_qmfpakgu6mowmslv4m5iy43t9`(`order_sn`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 233 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_pledge_log
-- ----------------------------
DROP TABLE IF EXISTS `loan_pledge_log`;
CREATE TABLE `loan_pledge_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键自增（保证金币种表）',
  `coin_unit` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '币种单位',
  `number` decimal(18, 8) NOT NULL COMMENT '数量',
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '状态【0:冻结；1:解冻】',
  `customer_id` bigint(20) NOT NULL COMMENT '交易人员',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `loan_guarantee_coin_id`(`coin_unit`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_prepayment
-- ----------------------------
DROP TABLE IF EXISTS `loan_prepayment`;
CREATE TABLE `loan_prepayment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `member_id` bigint(20) NOT NULL COMMENT '申请人id',
  `receiver_id` bigint(20) NOT NULL COMMENT '接收人id',
  `order_sn` bigint(20) NOT NULL COMMENT '订单号',
  `status` int(1) NOT NULL COMMENT '申请状态【0：未申请；1：申请中；2：申请成功；3：申请失败】',
  `reject_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '申请驳回理由',
  `submit_time` datetime(6) NOT NULL COMMENT '申请提交时间',
  `audit_time` datetime(6) NULL DEFAULT NULL COMMENT '审核时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_repayment
-- ----------------------------
DROP TABLE IF EXISTS `loan_repayment`;
CREATE TABLE `loan_repayment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '借贷还款表',
  `borrower_id` bigint(20) NOT NULL COMMENT '借贷者用户编号',
  `investor_id` bigint(20) NOT NULL COMMENT '投资者用户编号',
  `advertise_id` bigint(20) NOT NULL COMMENT '借贷广告编号',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '订单编号',
  `coin_unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '借币标识',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '还款状态：0、未还款；1、已还款 ；2、已完成还贷；3、还款失败',
  `money_rates` decimal(18, 8) NOT NULL COMMENT '支付利息',
  `investor_service_fee` decimal(18, 8) NULL DEFAULT 0.00000000 COMMENT '投资者服务费',
  `borrower_service_fee` decimal(18, 8) NULL DEFAULT NULL COMMENT '借款者服务费',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '还款时间',
  `over_due` int(1) NULL DEFAULT 0 COMMENT '是否逾期 0:否，1：是',
  `auto_liquidation` int(1) NULL DEFAULT 0 COMMENT '是否平仓 0:否，1：是',
  `early_repay` int(1) NULL DEFAULT 0 COMMENT '提前还款 0:否，1：是',
  `overdue_investor_service_fee` decimal(18, 0) NULL DEFAULT 0 COMMENT '逾期还款服务费',
  `overdue_money_rates` decimal(18, 0) NULL DEFAULT 0 COMMENT '逾期利息',
  `overdue_borrower_service_fee` decimal(18, 0) NULL DEFAULT 0 COMMENT '逾期借款服务费',
  `number` decimal(18, 8) NOT NULL COMMENT '交易数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1151 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_servie_fee_log
-- ----------------------------
DROP TABLE IF EXISTS `loan_servie_fee_log`;
CREATE TABLE `loan_servie_fee_log`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '借贷借款服务费支付记录表',
  `borrower_id` bigint(11) NULL DEFAULT NULL COMMENT '借款人编号',
  `log_time` datetime(0) NULL DEFAULT NULL COMMENT '记录时间',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '订单编号',
  `service_fee` decimal(18, 12) NULL DEFAULT NULL COMMENT '借款服务费，每日扣除',
  `bront_fronzen_balance` decimal(18, 12) NULL DEFAULT NULL COMMENT '支付手续费前冻结金额',
  `bake_fronzen_balance` decimal(18, 12) NULL DEFAULT NULL COMMENT '支付收费续费后冻结金额',
  `status` int(10) NULL DEFAULT NULL COMMENT '0支付成功，1支付失败',
  `coin_unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '支付币标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 124 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for loan_wallet
-- ----------------------------
DROP TABLE IF EXISTS `loan_wallet`;
CREATE TABLE `loan_wallet`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键自增（借贷钱包表）',
  `balance` decimal(18, 8) NOT NULL COMMENT '可用余额',
  `frozen_balance` decimal(18, 8) NOT NULL COMMENT '冻结余额',
  `is_lock` int(1) NOT NULL DEFAULT 0 COMMENT '是否锁定【0：不锁定；1：锁定】',
  `member_id` bigint(20) NOT NULL COMMENT '用户编号（用户表外键）',
  `coin_id` bigint(20) NOT NULL COMMENT '借贷币种编号（借贷币种外键）',
  `version` int(11) NULL DEFAULT 1,
  `unit` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '币种单位',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE,
  UNIQUE INDEX `member_unit`(`member_id`, `unit`) USING BTREE,
  INDEX `loan_wallet_coin_id`(`coin_id`) USING BTREE,
  CONSTRAINT `loan_wallet_coin_id` FOREIGN KEY (`coin_id`) REFERENCES `loan_coin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `loan_wallet_memeber_id` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2622 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ali_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `qr_code_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `appeal_success_times` int(11) NULL DEFAULT NULL,
  `appeal_times` int(11) NULL DEFAULT NULL,
  `application_time` datetime(0) NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `bank` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `branch` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `card_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `certified_business_apply_time` datetime(0) NULL DEFAULT NULL,
  `certified_business_check_time` datetime(0) NULL DEFAULT NULL,
  `certified_business_status` int(11) NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `first_level` int(11) NOT NULL,
  `google_date` datetime(0) NULL DEFAULT NULL,
  `google_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `google_state` int(11) NULL DEFAULT NULL,
  `id_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `inviter_id` bigint(20) NULL DEFAULT NULL,
  `jy_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `last_login_time` datetime(0) NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `district` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `login_count` int(11) NULL DEFAULT NULL,
  `margin` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `member_level` int(11) NULL DEFAULT NULL,
  `mobile_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `promotion_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `publish_advertise` int(11) NULL DEFAULT NULL,
  `real_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `real_name_status` int(11) NULL DEFAULT NULL,
  `registration_time` datetime(0) NULL DEFAULT NULL,
  `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `second_level` int(11) NOT NULL,
  `sign_in_ability` bit(1) NOT NULL DEFAULT b'1',
  `status` int(11) NULL DEFAULT NULL,
  `super_partner` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `third_level` int(11) NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `token_expire_time` datetime(0) NULL DEFAULT NULL,
  `transaction_status` int(11) NULL DEFAULT NULL,
  `transactions` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `qr_we_code_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `wechat` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `local` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `origin` tinyint(1) NULL DEFAULT NULL COMMENT '1代表IPcon手机端,2 ：HardID',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_ID`(`id`) USING BTREE,
  UNIQUE INDEX `UK_gc3jmn7c2abyo3wf6syln5t2i`(`username`) USING BTREE,
  INDEX `UK_mbmcqelty0fbrvxp1q58dn57t`(`email`) USING BTREE,
  INDEX `UK_10ixebfiyeqolglpuye0qb49u`(`mobile_phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 186 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_address
-- ----------------------------
DROP TABLE IF EXISTS `member_address`;
CREATE TABLE `member_address`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `delete_time` datetime(0) NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_application
-- ----------------------------
DROP TABLE IF EXISTS `member_application`;
CREATE TABLE `member_application`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `audit_status` int(11) NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `id_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `identity_card_img_front` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `identity_card_img_in_hand` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `identity_card_img_reverse` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `real_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `reject_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `member_id` bigint(20) NOT NULL,
  `card_type` tinyint(1) NULL DEFAULT NULL COMMENT '1:身份证 2：护照',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_application_config
-- ----------------------------
DROP TABLE IF EXISTS `member_application_config`;
CREATE TABLE `member_application_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `promotion_on` int(11) NULL DEFAULT NULL,
  `recharge_coin_on` int(11) NULL DEFAULT NULL,
  `transaction_on` int(11) NULL DEFAULT NULL,
  `withdraw_coin_on` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_bonus
-- ----------------------------
DROP TABLE IF EXISTS `member_bonus`;
CREATE TABLE `member_bonus`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `arrive_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `have_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `mem_bouns` decimal(18, 8) NULL DEFAULT NULL COMMENT '分红数量',
  `member_id` bigint(20) NULL DEFAULT NULL,
  `total` decimal(18, 8) NULL DEFAULT NULL COMMENT '当天总手续费',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_deposit
-- ----------------------------
DROP TABLE IF EXISTS `member_deposit`;
CREATE TABLE `member_deposit`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `amount` decimal(18, 8) NULL DEFAULT 0.00000000,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  `txid` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UKl2ibi99fuxplt8qt3rrpb0q4w`(`txid`, `address`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_exclusive_fee
-- ----------------------------
DROP TABLE IF EXISTS `member_exclusive_fee`;
CREATE TABLE `member_exclusive_fee`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL COMMENT '会员ID',
  `symbol` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '币种',
  `fee` decimal(18, 0) NOT NULL COMMENT '费率',
  `type` int(1) NOT NULL COMMENT '交易类型',
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `INDEX_EXCLUSIVE_FEE`(`id`) USING BTREE,
  UNIQUE INDEX `INDEX_member_id`(`member_id`, `symbol`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_legal_currency_wallet
-- ----------------------------
DROP TABLE IF EXISTS `member_legal_currency_wallet`;
CREATE TABLE `member_legal_currency_wallet`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `balance` decimal(26, 16) NULL DEFAULT NULL COMMENT '可用余额',
  `frozen_balance` decimal(26, 16) NULL DEFAULT NULL COMMENT '冻结余额',
  `is_lock` int(11) NULL DEFAULT 0 COMMENT '钱包不是锁定',
  `member_id` bigint(20) NULL DEFAULT NULL,
  `to_released` decimal(18, 8) NULL DEFAULT NULL COMMENT '待释放总量',
  `version` int(11) NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UKm68bscpof0bpnxocxl4qdnvbe`(`member_id`, `coin_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 367 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_level
-- ----------------------------
DROP TABLE IF EXISTS `member_level`;
CREATE TABLE `member_level`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_default` bit(1) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_level_fee
-- ----------------------------
DROP TABLE IF EXISTS `member_level_fee`;
CREATE TABLE `member_level_fee`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '币种',
  `member_level_id` bigint(20) NOT NULL COMMENT '会员等级表ID',
  `fee` decimal(18, 0) NOT NULL COMMENT '费率',
  `type` int(1) NOT NULL COMMENT '交易类型',
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `INDEX_LEVEL_FEE_ID`(`id`) USING BTREE,
  UNIQUE INDEX `INDEX_LEVEL_FEE_member_level_id`(`symbol`, `member_level_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_promotion
-- ----------------------------
DROP TABLE IF EXISTS `member_promotion`;
CREATE TABLE `member_promotion`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invitees_id` bigint(20) NULL DEFAULT NULL,
  `inviter_id` bigint(20) NULL DEFAULT NULL,
  `level` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_sign_record
-- ----------------------------
DROP TABLE IF EXISTS `member_sign_record`;
CREATE TABLE `member_sign_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19, 2) NULL DEFAULT NULL,
  `creation_time` datetime(0) NULL DEFAULT NULL,
  `coin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  `sign_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_transaction
-- ----------------------------
DROP TABLE IF EXISTS `member_transaction`;
CREATE TABLE `member_transaction`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `amount` decimal(26, 16) NULL DEFAULT NULL COMMENT '充币金额',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `discount_fee` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `fee` decimal(26, 16) NULL DEFAULT NULL,
  `flag` int(11) NOT NULL DEFAULT 0,
  `member_id` bigint(20) NULL DEFAULT NULL,
  `real_fee` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `symbol` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 241 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_wallet
-- ----------------------------
DROP TABLE IF EXISTS `member_wallet`;
CREATE TABLE `member_wallet`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `balance` decimal(26, 16) NULL DEFAULT NULL COMMENT '可用余额',
  `frozen_balance` decimal(26, 16) NULL DEFAULT NULL COMMENT '冻结余额',
  `is_lock` int(11) NULL DEFAULT 0 COMMENT '钱包不是锁定',
  `member_id` bigint(20) NULL DEFAULT NULL,
  `to_released` decimal(18, 8) NULL DEFAULT NULL COMMENT '待释放总量',
  `version` int(11) NOT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UKm68bscpof0bpnxocxl4qdnvbe`(`member_id`, `coin_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4871 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for otc_coin
-- ----------------------------
DROP TABLE IF EXISTS `otc_coin`;
CREATE TABLE `otc_coin`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `buy_min_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '买入广告最低发布数量',
  `is_platform_coin` int(11) NULL DEFAULT NULL,
  `jy_rate` decimal(18, 6) NULL DEFAULT NULL COMMENT '交易手续费率',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name_cn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sell_min_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '卖出广告最低发布数量',
  `sort` int(11) NOT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for otc_coin_subscription
-- ----------------------------
DROP TABLE IF EXISTS `otc_coin_subscription`;
CREATE TABLE `otc_coin_subscription`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `coin_id` int(11) NULL DEFAULT NULL COMMENT '法币ID',
  `sort` int(11) NOT NULL,
  `origin` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for otc_order
-- ----------------------------
DROP TABLE IF EXISTS `otc_order`;
CREATE TABLE `otc_order`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `advertise_id` bigint(20) NOT NULL,
  `advertise_type` int(11) NOT NULL,
  `ali_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `qr_code_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `bank` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `branch` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `card_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `cancel_time` datetime(0) NULL DEFAULT NULL,
  `commission` decimal(18, 8) NULL DEFAULT NULL COMMENT '手续费',
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `customer_id` bigint(20) NOT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `customer_real_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `max_limit` decimal(18, 2) NULL DEFAULT NULL COMMENT '最高交易额',
  `member_id` bigint(20) NOT NULL,
  `member_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `member_real_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `min_limit` decimal(18, 2) NULL DEFAULT NULL COMMENT '最低交易额',
  `money` decimal(18, 2) NULL DEFAULT NULL COMMENT '交易金额',
  `number` decimal(18, 8) NULL DEFAULT NULL COMMENT '交易数量',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pay_mode` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pay_time` datetime(0) NULL DEFAULT NULL,
  `price` decimal(18, 2) NULL DEFAULT NULL COMMENT '价格',
  `release_time` datetime(0) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `time_limit` int(11) NULL DEFAULT NULL,
  `version` bigint(20) NULL DEFAULT NULL,
  `qr_we_code_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `wechat` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `coin_id` bigint(20) NOT NULL,
  `pay_type` tinyint(1) NULL DEFAULT NULL COMMENT '确认付款方式 0：支付宝 1：微信 2：银联',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_qmfpakgu6mowmslv4m5iy43t9`(`order_sn`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 133 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for reward_activity_setting
-- ----------------------------
DROP TABLE IF EXISTS `reward_activity_setting`;
CREATE TABLE `reward_activity_setting`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for reward_promotion_setting
-- ----------------------------
DROP TABLE IF EXISTS `reward_promotion_setting`;
CREATE TABLE `reward_promotion_setting`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `effective_time` int(11) NOT NULL,
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for reward_record
-- ----------------------------
DROP TABLE IF EXISTS `reward_record`;
CREATE TABLE `reward_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '数目',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sign
-- ----------------------------
DROP TABLE IF EXISTS `sign`;
CREATE TABLE `sign`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19, 2) NULL DEFAULT NULL,
  `creation_time` datetime(0) NULL DEFAULT NULL,
  `end_date` date NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `coin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_advertise
-- ----------------------------
DROP TABLE IF EXISTS `sys_advertise`;
CREATE TABLE `sys_advertise`  (
  `serial_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `end_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `link_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `start_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` int(11) NOT NULL,
  `sys_advertise_location` int(11) NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`serial_number`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_help
-- ----------------------------
DROP TABLE IF EXISTS `sys_help`;
CREATE TABLE `sys_help`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `is_top` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `sys_help_classification` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_sms
-- ----------------------------
DROP TABLE IF EXISTS `tb_sms`;
CREATE TABLE `tb_sms`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `key_secret` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `sign_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `sms_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `sms_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `template_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for transfer_address
-- ----------------------------
DROP TABLE IF EXISTS `transfer_address`;
CREATE TABLE `transfer_address`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `min_amount` decimal(18, 2) NULL DEFAULT NULL COMMENT '最低转账数目',
  `status` int(11) NULL DEFAULT NULL,
  `transfer_fee` decimal(18, 6) NULL DEFAULT NULL COMMENT '转账手续费率',
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for transfer_other_record
-- ----------------------------
DROP TABLE IF EXISTS `transfer_other_record`;
CREATE TABLE `transfer_other_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id_from` bigint(20) NULL DEFAULT NULL COMMENT '交易人员ID',
  `member_id_to` bigint(20) NULL DEFAULT NULL COMMENT '交易人员ID',
  `wallet_id_from` bigint(20) NOT NULL COMMENT '币币账户id',
  `wallet_id_to` bigint(20) NOT NULL COMMENT '法币账户ID',
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '币种ID',
  `total_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '数量',
  `fee` decimal(18, 8) NULL DEFAULT NULL COMMENT '手续费',
  `arrived_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '预计到账数量',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `deal_time` datetime(0) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL COMMENT 'type=0 失败 type=1 成功',
  `type` int(11) NULL DEFAULT NULL COMMENT 'type=IpCOM手机端转账',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for transfer_record
-- ----------------------------
DROP TABLE IF EXISTS `transfer_record`;
CREATE TABLE `transfer_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `amount` decimal(19, 2) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `fee` decimal(18, 8) NULL DEFAULT NULL COMMENT '手续费',
  `member_id` bigint(20) NULL DEFAULT NULL,
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for transfer_self_record
-- ----------------------------
DROP TABLE IF EXISTS `transfer_self_record`;
CREATE TABLE `transfer_self_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `legalcurrency_id` bigint(20) NULL DEFAULT NULL COMMENT '法币账户ID',
  `wallet_id` bigint(20) NULL DEFAULT NULL COMMENT '币币账户id',
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '币种ID',
  `total_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '数量',
  `fee` decimal(18, 8) NULL DEFAULT NULL COMMENT '手续费',
  `arrived_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '预计到账数量',
  `member_id` bigint(20) NULL DEFAULT NULL COMMENT '交易人员ID',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `deal_time` datetime(0) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL COMMENT '   // type=0 币币转入法币账户 type=1 币币转出法币账户 \r\n    // 3：币币转到借贷 4：借贷转到币币\r\n // 5：法币转到借贷 6：借贷转到法币',
  `status` int(11) NULL DEFAULT NULL COMMENT 'type=0 失败 type=1 成功',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for website_information
-- ----------------------------
DROP TABLE IF EXISTS `website_information`;
CREATE TABLE `website_information`  (
  `id` bigint(20) NOT NULL,
  `address_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `copyright` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `other_information` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `postcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for withdraw_record
-- ----------------------------
DROP TABLE IF EXISTS `withdraw_record`;
CREATE TABLE `withdraw_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `arrived_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '预计到账数量',
  `can_auto_withdraw` int(11) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `deal_time` datetime(0) NULL DEFAULT NULL,
  `fee` decimal(18, 8) NULL DEFAULT NULL COMMENT '手续费',
  `is_auto` int(11) NULL DEFAULT NULL,
  `member_id` bigint(20) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `total_amount` decimal(18, 8) NULL DEFAULT NULL COMMENT '申请总数量',
  `transaction_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `admin_id` bigint(20) NULL DEFAULT NULL,
  `coin_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
